module ChefAPI
  class Resource::Group < Resource::Base
    collection_path '/groups'

    schema do
      attribute :groupname, type: String, primary: true, required: true
      attribute :json_class, type: String, default: "Chef::Group"
      attribute :name,      type: String
      attribute :orgname,   type: String
      attribute :actors,    type: Array, default: []
      attribute :users,     type: Array, default: []
      attribute :clients,   type: Array, default: []
      attribute :groups,    type: Array, default: []
    end

    # GET/PUT json has a different format. narf.
    def to_json(*)
      hash = self.to_hash
      users = hash.delete(:users)
      groups = hash.delete(:groups)
      clients = hash.delete(:clients)
      hash[:actors] = {
        users: users,
        groups: groups,
        clients: clients
      }
      JSON.fast_generate(hash)
    end
  end
end

